import java.util.ArrayList;

public class HumanRepo {
    ArrayList<Man> mass = new ArrayList<Man>();

    public static Man funcp1 (Integer Age, String SecondName) {
        Man p2 = new Man();
            p2.Age = Age;
            p2.SecondName = SecondName;
        return p2;
    }

    public void main(String[] args){
        mass.add(funcp1(12,"Nick"));
        mass.add(funcp1(11,"Samuel"));
        mass.add(funcp1(14,"John"));
        System.out.println(mass);

        //1:12, 2:Nick, 3:11, 4:Samuel, 5:14, 6:John
        //1:(12, Nick), 2:(11, Samuel), 3:(14, John)
    }
}